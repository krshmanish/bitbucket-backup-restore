echo 'This script will restore all the repository :)'

cd $PWD/'clonedbackups/'

# Find the latest directory for backup
latestDir=$(ls -td -- */ | head -n 1 | cut -d'/' -f1)
cd $latestDir

echo 'Latest direcotry for backup is '$PWD

# Ask user to select Versioning tool from list
echo 'Select Versioning tool from the list :'
echo '1 ) Github          2 ) Bit Bucket'
echo 'Please select the number : '
read versioningOption

# use case statement to make decision for rental
case $versioningOption in
    1)
        baseUrl=github.com
    optionName=Github;;
    2)
        baseUrl=bitbucket.org
    optionName='Bit Bucket';;
    *)
        echo "Please enter number from the list!!"
    exit;;
esac

echo Enter you $optionName user name :
read userName

echo Enter your $optionName password :
stty -echo
read password;
stty echo

for repo in */ ; do
    repoName=${repo%/}

    cd $repoName
    echo 'Creating new repository with name of : '$repoName

    if [ $versioningOption -eq 1 ];
    then
        curl -u $userName':'$password https://api.$baseUrl/user/repos -d '{"name":"'$repoName'"}'
    elif [ $versioningOption -eq 2 ];
    then
        curl -sS --location --fail -i -X POST \
        -u $userName:$password \
        -d "{ \"scm\": \"git\" }" \
        https://api.$baseUrl/2.0/repositories/{$userName}/{$repoName}
    fi
    wait

    for remote in `git branch -r | grep -v master `;
    do
        git checkout --track $remote
        wait
    done

    git remote set-url origin git@$baseUrl:$userName/$repoName.git
    wait

    git push -u --mirror
    wait

    cd ..
done
