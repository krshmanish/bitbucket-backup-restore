#!/bin/bash

# activate virtual environment for bitbucket backup script
DIR="/home/$USER_NAME/BitBucketBackup"
if [ -d "$DIR" ]; then
    echo "Directory found!"
else
    echo "Backup folder not found!"
    exit 1
fi

cd "$DIR"
source bin/activate

# execute backup script to do a backup to the defined folder (see bbbackup.cfg which folder and which config the app will run with)
python bbbackup.py --configuration "$DIR"/bbbackup.cfg --notify --backup
