# Go to backup folder
cd $PWD/'clonedbackups/'

# Find the latest directory for backup
latestDir=$(ls -td -- */ | head -n 1 | cut -d'/' -f1)
cd $latestDir

# Ask user to select Versioning tool from list
echo 'Select Versioning tool from the list :'
echo '1 ) Github          2 ) Bit Bucket'
echo 'Please select the number : '
read versioningOption

# use case statement to make decision for rental
case $versioningOption in
    1)
        baseUrl=github.com
        optionName=Github;;
    2)
        baseUrl=bitbucket.org
        optionName='Bit Bucket';;
    *)
        echo "Please enter number from the list!!"
        exit;;
esac


# Ask user to enter folder name in which he want to restore
echo Please enter cloned repository\'s folder name :
read folderName

echo Enter you $optionName user name :
read userName

echo Enter your $optionName password :
stty -echo
read password;
stty echo

echo 'Creating new repository in '$optionName' with name of '$folderName

if [ $versioningOption -eq 1 ];
then
    curl -u $userName':'$password https://api.$baseUrl/user/repos -d '{"name":"'$folderName'"}'
elif [ $versioningOption -eq 2 ];
then
    echo Is this a private repository ?
    read isPrivate

    curl -sS --location --fail -i -X POST \
    -u $userName:$password \
    -d "{ \"scm\": \"git\", \"is_private\": \"$isPrivate\" }" \
    https://api.$baseUrl/2.0/repositories/{$userName}/{$folderName}
fi

cd $folderName
echo Current working directory : $PWD

for remote in `git branch -r | grep -v master `;
do
    git checkout --track $remote
done

# git remote set-url origin git@github.com:$userName/$folderName.git
git remote set-url origin git@$baseUrl:$userName/$folderName.git
wait

git push -u --mirror
