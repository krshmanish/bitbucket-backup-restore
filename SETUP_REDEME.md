# Setup for backup repositories
Bitbucket automatic backup solution providing Cloud-to-Local and Cloud-to-Cloud repository cloning written in Python

## Operation
Please install this script on a UNIX machine which is capable of executing scripts in an automated way. Have a closer look at the `bbbackup.sh` which is prepared to be called by a cronjob.

Following files need to be deployed for backup to a target machine for installation:

* `bbbackup.py`
* `requirements.txt`
* `bbbackup.sh` (Provide `$USER_NAME` of your PC)
* `bbbackup.cfg` (for setup your credentials and other information : Slack details is require)

Following file need to be deployed for restore a particular repository to github repository.

* `restore.sh` (Need to provide selected repository folder name then select the option (Github or Bit Bucket) and provide user name and password)

Following file need to be deployed for restore all repositories to github repository.

* `all-restore.sh` (Select the option (Github or Bit Bucket) and provide user name and password, It will automatically find the latest folder in which all repository cloned)

Following file need to be deployed for delete all repositories from github.

* `delete-repos.sh` (It requires `repos.txt` file which include all the repository name and also requires a github token)
### repos.txt

```
userName/repo-1
userName/repo-2
userName/repo-3
...
...
...
userName/repo-n
```

## Features

The script supports following features:

* Configure userid/teamname (password optional!) for accessing BitBucket repositories
* Configure slack so the solution can message/report via slack channel
* Execute full backup of all repositories belonging to a team or user
* Notify about success/warnings/failure via slack
* Execute post-backup analysis of existing backups (specific days & all backups available)
* Automatically rotate backups in a certain timespan, e.g. 14 days
* Monitor the free space remaning on the volume and configure limits for space usage
* Configure retry-limit for retrys of failed cloning of repos
* Configure error/fail-limit-threshold when to report the whole @channel in slack

* Restore single repository using `restore.sh` script.
* Restore all the repository using `all-restore.sh` script.
* Delete all the github repository using `delete-repos.sh` script.

## Requirements

The script needs a certain environment of python modules to execute its tasks. For details see `requirements.txt` to install needed python libraries using `pip install -r requirements.txt` on the commandline.

* Python 3 (use a *virtual environment* with `python3 -m venv <myfolder>`)
* **slackclient (v2.0.0)** — used to support messaging via Slack
* **GitPython (v2.1.11)** — used to support repo cloing via git
* **keyring (v19.0.1)** — used to store credentials and API keys/secrets on local OS's
* **rauth (v0.7.3)** — used to support OAuth2 requests against BitBucket API
* **configparser (v3.7.4)** — used to enable usage of config-file

#### Python 3 ####

check if you have Python 3 installed
`python3 --version`

if no Python 3 is available on the machine, please install it using apt
`sudo apt update`
`sudo apt install python3`

create a virtual environment with Python 3
`python3 -m venv BitBucketBackup`

change into the created environment folder
`cd BitBucketBackup`

copy following files into the current folder:

* `requirements.txt`
* `bbbackup.py`
* `bbbackup.sh` (Provide `$USER_NAME` of your PC)
* `bbbackup.cfg`
* `restore.sh` (Optional: restore single respository)
* `all-restore.sh` (Optional: restore all the repository)
* `delete-repos.sh` (Optional: delete all the github repository)
* `repos.txt` (Required when `delete-repos.sh` added)

check if python 3 is now available
`python --version` (this should give something like: "Python 3.7.1")

ensure you have the latest pip python module manager installed
`pip install --upgrade pip` (use sudo if needed)

install python modules required
`pip install -r requirements.txt` or `pip3 install -r requirements.txt`

# How to run these scritp

* First configure the `bbbacku.cfg` file
* For backup : Run `bbbackup.sh` with `bash bbbacku.sh` command.
* For restore single repository : Run `restore.sh` with `sh restore.sh` command.
* For restore all repository : Run `all-restore.sh` with `sh all-restore.sh` command.
* For delete all repository from github : Run `delete-repos.sh` with `sh delete-repos.sh` command.
